create table employees (
	id serial primary key,
	name text,
	gender text,
	birthday date,
	recommendation text
);