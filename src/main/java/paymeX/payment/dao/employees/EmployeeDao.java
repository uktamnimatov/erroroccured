package paymeX.payment.dao.employees;

import java.util.List;

public interface EmployeeDao {

    Employee getById(Integer id);

    List<Employee> getList();

    void deleteById(Integer id);

    void create(Employee employee);

    void update(Employee employee);

}
