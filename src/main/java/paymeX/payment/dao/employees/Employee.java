package paymeX.payment.dao.employees;

import java.sql.Date;

public class Employee {

    private Integer id;
    private String name;
    private String gender;
    private Date birthday;
    private String recommendation;

    public Integer getId() {
        return id;
    }

    public Employee setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Employee setName(String name) {
        this.name = name;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public Employee setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public Date getBirthday() {
        return birthday;
    }

    public Employee setBirthday(Date birthday) {
        this.birthday = birthday;
        return this;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public Employee setRecommendation(String recommendation) {
        this.recommendation = recommendation;
        return this;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", birthday=" + birthday +
                ", recommendation='" + recommendation + '\'' +
                '}';
    }
}
