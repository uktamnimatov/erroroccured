package paymeX.payment.dao.employees;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import paymeX.payment.UserData2;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public EmployeeDaoImpl setJdbcTemplate(DataSource newPostgres, JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = new JdbcTemplate(newPostgres);
        return this;
    }

    @Override
    public Employee getById(Integer id) {
        String sql = "select * from employees where id = ?";
        Employee employee = new Employee();
        jdbcTemplate.query(sql, preparedStatement -> {
            preparedStatement.setObject(1, id);
        }, new ResultSetExtractor<Object>() {
            @Override
            public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if (resultSet.next())
                    return employee
                            .setName(resultSet.getString("name"))
                            .setGender(resultSet.getString("gender"))
                            .setRecommendation(resultSet.getString("recommendation"))
                            .setId(resultSet.getInt("id"));
                return null;
            }
        });
        return employee;
    }

    @Override
    public List<Employee> getList() {
        String sql = "select * from employees";
        List<Employee> list = new ArrayList<>();
        jdbcTemplate.query(sql, (resultSet, i) -> {
            list.add(new Employee()
                    .setId(resultSet.getInt("id"))
                    .setName(resultSet.getString("name"))
                    .setBirthday(resultSet.getDate("birthday"))
                    .setGender(resultSet.getString("gender"))
                    .setRecommendation(resultSet.getString("recommendation")));
            return null;
        });
        return list;
    }

    @Override
    public void deleteById(Integer id) {
        String sql = "delete from employees where id = ?";
        jdbcTemplate.update(sql, preparedStatement -> {
            preparedStatement.setObject(1, id);
        });
    }

    @Override
    public void create(Employee employee) {
        String sql = "insert into employees (name,gender,recommendation) values (?,?,?)";
        jdbcTemplate.update(sql, preparedStatement -> {
            preparedStatement.setObject(1, employee.getName());
            preparedStatement.setObject(2, employee.getGender());
            preparedStatement.setObject(3, employee.getRecommendation());
        });
    }

    @Override
    public void update(Employee employee) {
        System.out.println(employee.toString());

    }
}
