package paymeX.payment.service.employees;

import paymeX.payment.dao.employees.Employee;

import java.util.List;

public interface EmployeeService {

    Employee getById(Integer id);

    List<Employee> getList();

    void deleteById(Integer id);

    void create(Employee employee);

    void update(Employee employee);
}
