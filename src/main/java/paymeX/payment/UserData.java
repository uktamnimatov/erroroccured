package paymeX.payment;

public class UserData {
    private String userName;
    private String userPassword;
    private int userID;

    public String getUserName() {
        return userName;
    }

    public UserData setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public UserData setUserPassword(String userPassword) {
        this.userPassword = userPassword;
        return this;
    }

    public int getUserID() {
        return userID;
    }

    public UserData setUserID(int userID) {
        this.userID = userID;
        return this;
    }
}
