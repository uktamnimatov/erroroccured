package paymeX.payment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import paymeX.payment.Repo;
import paymeX.payment.UserData;

import java.util.ArrayList;
import java.util.List;


@Controller
public class PaymexController {

    List<Object> listOfUsers = new ArrayList<>();

    Repo repo;

    @Autowired
    public PaymexController setRepo(Repo repo) {
        this.repo = repo;
        return this;
    }

    @GetMapping(value = "/index.html")
    public String showDashboard() {

        return "index";
    }

    @GetMapping(value = "/charts.html")
    public String showCharts() {
        return "charts";
    }

    @GetMapping(value = "/tables.html")
    public String showTables(@ModelAttribute UserData userData, Model model) {
        model.addAttribute(userData);
        model.addAttribute("users", listOfUsers);
        return "tables";
    }

    @GetMapping(value = "/forms.html")
    public String showForms() {
        return "forms";
    }

    @GetMapping(value = "/login.html")
    public String showLoginPage() {
        return "login";
    }

    @PostMapping(value = "/doRegistration")
    public String doRegistration(@ModelAttribute UserData userData, Model model) {
        listOfUsers.add(userData);
        userData.setUserID(listOfUsers.size());
        model.addAttribute("userName", userData.getUserName());
        System.out.println(userData.getUserName());
        System.out.println(userData.getUserPassword());
        System.out.println(userData.getUserID());
        return "index";
    }
}
