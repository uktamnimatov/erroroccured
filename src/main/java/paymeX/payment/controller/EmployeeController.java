package paymeX.payment.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import paymeX.payment.dao.employees.Employee;
import paymeX.payment.service.employees.EmployeeService;

@Controller
public class EmployeeController {

    private EmployeeService employeeService;

    @Autowired
    public EmployeeController setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
        return this;
    }

    @GetMapping(value = "/employee")
    public String showEmployees(Model model) {
        model.addAttribute("empList",employeeService.getList());
        return "employees";
    }

    @GetMapping(value = "/employee/add")
    public String createEmployee(Model model) {
        model.addAttribute("employee",new Employee());
        return "employee_create";
    }

    @PostMapping(value = "/doregister")
    public String doRegister(Employee employee, Model model) {
        employeeService.create(employee);
        model.addAttribute("empList",employeeService.getList());
        return "employees";
    }

    @GetMapping(value = "/employee/edit/{id}")
    public String createEmployee(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("employee",employeeService.getById(id));
        System.out.println(employeeService.getById(id).toString());
        return "employee_update";
    }

    @PostMapping(value = "/update")
    public String update(Employee employee, Model model) {
        employeeService.update(employee);
        System.out.println("update bolmoqchi employee" + employee);
        model.addAttribute("empList",employeeService.getList());
        return "employees";
    }

    @GetMapping(value = "/employee/delete/{id}")
    public String deleteEmployee(@PathVariable("id") Integer id, Model model) {
        employeeService.deleteById(id);
        model.addAttribute("empList",employeeService.getList());
        return "employees";
    }
}
